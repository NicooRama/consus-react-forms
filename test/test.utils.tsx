import { theme } from '../src/utils/theme'
import { render as testingLibraryRender } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { MemoryRouter } from 'react-router-dom'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'

const TestThemeProvider = ({ children }: any) =>
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>

export const FormikWrapper = ({ children, options }: any) => {
  return (
    <TestThemeProvider>
      <Formik
        enableReinitialize={true}
        onSubmit={() => {
        }}
        {...options}
      >
        {() => (
          <Form>
            {children}
          </Form>
        )}
      </Formik>
    </TestThemeProvider>

  )
}

export const RouterProvider = ({ children }: any) => (
    <MemoryRouter initialEntries={['/my/initial/route']}>
      {children}
    </MemoryRouter>
)

export const render = (component: any, options = {}) => {
  return testingLibraryRender(component, { wrapper: TestThemeProvider, ...options })
}

export const renderWithFormik = (component: any, options: any = {
  formikOptions: {
    initialValues: {
      test: '',
      multiTest: [],
      booleanTest: false,
    },
    validationSchema: Yup.object().shape({
      test: Yup.string().required('Ingrese un nombre'),
      multiTest: Yup.array(),
    }),
  },
  withRouterWrapper: false
}) => {
  const { formikOptions } = options;
  // @ts-ignore
  delete options.formikOptions;
  const formikWrapper = ({children}: any) => <FormikWrapper options={formikOptions}>
    {children}
  </FormikWrapper>;
  const wrappers = [formikWrapper];
  if(options.withRouterWrapper) {
    wrappers.push(RouterProvider);
  }
  delete options.withRouterWrapper;
  return testingLibraryRender(component, { wrapper: createWrapper(...wrappers), ...options })
}

const createWrapper = (...wrappers: any[]) => {
  return ({ children }: any) => {
    return wrappers.reduce((acc, Wrapper) => {
      return <Wrapper>{acc}</Wrapper>
    }, children)
  }
}

export const renderWithRouter = (component: any, options = {}) => {
  return testingLibraryRender(component, { wrapper: createWrapper(TestThemeProvider, RouterProvider), ...options })
}
