import {StrUtils} from "@consus/js-utils";

export const decimalInputOnChange = (event: any) =>{
    let value = event.target.value;
    let lastCharacter = StrUtils.lastCaracter(value);
    if(lastCharacter==="."){
        lastCharacter = ",";
        value = StrUtils.removeLastChar(value)+",";
        event.target.value = value;
    }
    if((!StrUtils.isCharNumber(lastCharacter) && lastCharacter!==",") || StrUtils.count(value,",")>1){
        event.target.value = StrUtils.removeLastChar(value);
    }
};

export const capitalizeInputOnChange = (event: any) =>{
    event.target.value = StrUtils.firstCapitalize(event.target.value);
};

export const handleNumericChange = (setFieldValue: any) => {
    return (e: any) => {
        decimalInputOnChange(e);
        setFieldValue(e.target.name, e.target.value);
    }
};

export const handleCapitalizeChange = (setFieldValue: any) => {
    return (e: any) => {
        capitalizeInputOnChange(e);
        setFieldValue(e.target.name, e.target.value);
    }
};
