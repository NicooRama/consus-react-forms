import React from 'react';
import {getIn} from 'formik';

export const showFormErrors = (touched: object, errors: object, submitCount: number, name: string) => (getIn(touched,name) || submitCount > 0) && getIn(errors, name)
