import type {Meta, StoryObj} from '@storybook/react';
import {FormRichInputSkeleton} from './FormRichInputSkeleton';

const meta: any = {
    title: 'Inputs/FormRichInput/FormRichInputSkeleton',
    component: FormRichInputSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof FormRichInputSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
