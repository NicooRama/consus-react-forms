import type {Meta, StoryObj} from '@storybook/react';
import {FormRichInput} from './FormRichInput';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormRichInput/FormRichInput',
    component: FormRichInput,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormRichInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test'
    },
};
