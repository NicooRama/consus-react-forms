import React from "react"

import { FormControlWrapper } from "../form-control-wrapper/FormControlWrapper";
import {RichInput} from '@consus/react-ui';
import {FormControlProps} from "../formControl.props";

export type FormRichInputProps = Omit<FormControlProps, 'fast'>;

export const FormRichInput: React.FC<FormRichInputProps> = ({
                                name,
                                className = "",
                                label,
                                ...props
                              }) => {
  return (<FormControlWrapper name={name} label={label} className={className}>
    {({form, field}: any) => (
      <RichInput onChange={form.handleChange(name)}
                 //@ts-ignore
                 onBlur={form.handleBlur(name)}
                 initialValue={field.value}
                 name={name}
                 {...props}
      />
    )}
  </FormControlWrapper>)
};
