import {FormRichInputSkeleton} from "./FormRichInputSkeleton";
import {render} from "../../../test/test.utils";

describe('<FormRichInputSkeleton />', () => {
    it('renders <FormRichInputSkeleton /> successfully', () => {
        const {getByTestId} = render(<FormRichInputSkeleton data-testid={'form-rich-input-skeleton'}/>)
        expect(getByTestId('form-rich-input-skeleton')).toBeInTheDocument();
    });
});
    