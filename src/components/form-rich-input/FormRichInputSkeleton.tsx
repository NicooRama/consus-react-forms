import React from "react"
import { SkeletonLabel, SkeletonSquare } from '@consus/react-ui'

export interface FormRichInputSkeletonProps {
  labelWidth?: number | string;
  showLabel?: boolean;
  [props: string]: any;
}

export const FormRichInputSkeleton: React.FC<FormRichInputSkeletonProps> = ({ labelWidth = 150, showLabel = true, ...props }) => {
  return (<div {...props}>
    {
      showLabel && <SkeletonLabel width={labelWidth}/>
    }
    <SkeletonSquare  height={'165px'} />
  </div>)
}
