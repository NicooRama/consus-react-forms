import {FormRichInput} from "./FormRichInput";
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormRichInput />', () => {
    it('renders <FormRichInput /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormRichInput data-testid={'form-rich-input'} name={'test'}/>)
        expect(getByTestId('form-rich-input')).toBeInTheDocument();
    });
});
