import {FormErrorMessage} from "./FormErrorMessage";
import {render} from "../../../test/test.utils";

describe('<FormErrorMessage />', () => {
    it('renders <FormErrorMessage /> successfully', () => {
        const {getByTestId} = render(<FormErrorMessage data-testid={'form-error-message'}>An error message</FormErrorMessage>)
        expect(getByTestId('form-error-message')).toBeInTheDocument();
    });
});
