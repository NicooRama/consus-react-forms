import type {Meta, StoryObj} from '@storybook/react';
import {FormErrorMessage} from './FormErrorMessage';

const meta: any = {
    title: 'Commons/FormErrorMessage',
    component: FormErrorMessage,
    tags: ['autodocs'],
} satisfies Meta<typeof FormErrorMessage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'An error message'
    },
};
