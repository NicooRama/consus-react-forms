import React from 'react';
import {Text} from '@consus/react-ui';
import styled from 'styled-components';

const StyledText = styled(Text)`
    margin-top: ${({theme}: any) => theme.spacing.xxs}px;
`

export interface FormErrorMessageProps {
    children: string
    [props: string]: any
}

export const FormErrorMessage: React.FC<FormErrorMessageProps> = ({children, ...props}) => {
    return (<StyledText color={'error.normal'} size={'xs'} {...props}>{children}</StyledText>)
};
