import type {Meta, StoryObj} from '@storybook/react';
import {FormRadioGroupSkeleton} from './FormRadioGroupSkeleton';

const meta: any = {
    title: 'Form options/FormRadioGroup/FormRadioGroupSkeleton',
    component: FormRadioGroupSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof FormRadioGroupSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
