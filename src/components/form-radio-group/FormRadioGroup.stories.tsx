import type {Meta, StoryObj} from '@storybook/react';
import {FormRadioGroup} from './FormRadioGroup';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Form options/FormRadioGroup/FormRadioGroup',
    component: FormRadioGroup,
    tags: ['autodocs'],
    decorators: [FormikDecorator()]
} satisfies Meta<typeof FormRadioGroup>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test',
        options: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"]
    },
};
