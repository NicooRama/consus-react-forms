import {FormRadioGroup} from "./FormRadioGroup";
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormRadioGroup />', () => {
    it('renders <FormRadioGroup /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormRadioGroup data-testid={'form-radio-group'}
                                                        name={'test'}
                                                        options={["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"]}
        />)
        expect(getByTestId('form-radio-group')).toBeInTheDocument();
    });
});
