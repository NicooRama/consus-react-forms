import React from 'react'
import { mobileMediaQuery, SkeletonButton, SkeletonLabel } from '@consus/react-ui'
import styled from 'styled-components'

export const ButtonContainer = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.xs}px;
  flex-wrap: wrap;

  ${mobileMediaQuery} {
    div {
      width: 100%;
    }
  }
`

export interface FormRadioGroupSkeletonProps {
  labelWidth?: number | string;
  showLabel?: boolean;
  quantity?: number;
  buttonWidth?: string;
  [props: string]: any;
}

export const FormRadioGroupSkeleton = ({
                                         showLabel = true,
                                         labelWidth = 150,
                                         quantity = 5,
                                         buttonWidth = '125px',
  ...props
                                       }: FormRadioGroupSkeletonProps) => {

  return (<div {...props}>
    {
      showLabel && <SkeletonLabel width={labelWidth} />
    }
    <ButtonContainer>
      {
        Array.from(Array(quantity).keys()).map((index: number) => <SkeletonButton width={buttonWidth}
                                                                                  key={`button${index}`} />)
      }
    </ButtonContainer>
  </div>)
}
