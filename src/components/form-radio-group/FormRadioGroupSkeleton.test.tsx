import {FormRadioGroupSkeleton} from "./FormRadioGroupSkeleton";
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormRadioGroupSkeleton />', () => {
    it('renders <FormRadioGroupSkeleton /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormRadioGroupSkeleton data-testid={'form-radio-group-skeleton'}/>)
        expect(getByTestId('form-radio-group-skeleton')).toBeInTheDocument();
    });
});
