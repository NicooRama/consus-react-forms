import React from "react"
import PropTypes from 'prop-types';
import {RadioButtonGroup} from "@consus/react-ui";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";
import {FormControlProps} from "../formControl.props";

export interface FormRadioGroupProps extends FormControlProps {
  options: any[],
}

export const FormRadioGroup: React.FC<FormRadioGroupProps> = ({name, label, options, className = "", fast, ...props}) => {
  return (<FormControlWrapper name={name} label={label} className={className} fast={fast}>
    {({form, field}: any) => (
      <RadioButtonGroup options={options}
                        onCheck={(option) => form.setFieldValue(name, option)}
                        selected={field.value}
                        {...props}
      />
    )}
  </FormControlWrapper>)
};

FormRadioGroup.propTypes = {
  className: PropTypes.string,
};
