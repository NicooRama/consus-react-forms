import PropTypes from 'prop-types';

export interface FormControlProps {
    name: string,
    label?: string | any,
    className?: string,
    fast?: boolean,
    [props: string]: any;
}
