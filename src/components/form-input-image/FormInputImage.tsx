import React from 'react'
import { mobileMediaQuery } from '@consus/react-ui'
import styled from 'styled-components'
import { getIn, useFormikContext } from 'formik'
import { FormInput } from '../form-input/FormInput'
import { FormInputFile } from '../form-input-file/FormInputFile'

const Container = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.sm}px;
  align-items: flex-end;

  div:first-of-type {
    flex-grow: 1;
  }

  ${mobileMediaQuery} {
    span {
      &.with-children {
        width: auto;
        height: 24px;
      }
    ;

      .children {
        display: none;
      }
    ;
    }
  }
`

export interface FormInputImageProps {
  input?: {
    name: string;
    label: string;
  },
  file?: {
    name: string;
    label: string;
  },
  maxSize?: number;
  allowUploadFile?: boolean;
  [props: string]: any;
}

export const FormInputImage: React.FC<FormInputImageProps> = ({
                                                                input = {
                                                                  label: 'Url Imagen',
                                                                  name: 'image.url',
                                                                },
                                                                file = {
                                                                  label: 'Subir imagen',
                                                                  name: 'image.file',
                                                                },
                                                                maxSize,
                                                                allowUploadFile = true,
                                                                ...props
                                                              }) => {
  const { values } = useFormikContext<any>()

  return (<Container {...props}>
    <FormInput label={input.label}
               name={input.name}
               disabled={!!getIn(values, file.name)}
    />
    {
      allowUploadFile && <FormInputFile label={file.label} name={file.name} maxSize={maxSize} />
    }
  </Container>)
}
