import {FormInputImage} from "./FormInputImage";
import { render, renderWithFormik } from '../../../test/test.utils'
import * as Yup from 'yup';

describe('<FormInputImage />', () => {
    it('renders <FormInputImage /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormInputImage data-testid={'form-input-image'}/>, {
            formikOptions: {
                initialValues: {
                    image: {
                        url: '',
                        file: null,
                    }
                },
                validationSchema: Yup.object().shape({
                }),
            },
        })
        expect(getByTestId('form-input-image')).toBeInTheDocument();
    });
});
