import type {Meta, StoryObj} from '@storybook/react';
import {FormInputImage} from './FormInputImage';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormInputImage',
    component: FormInputImage,
    tags: ['autodocs'],
    decorators: [FormikDecorator({
        extraValues: {
            image: {
                url: '',
                file: null,
            }
        }
    })],
} satisfies Meta<typeof FormInputImage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
