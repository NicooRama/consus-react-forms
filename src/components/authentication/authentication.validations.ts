import * as Yup from 'yup';

export const passwordValidation = (minMessage: string, maxMessage: string, emptyMessage: string) => Yup.string().min(6, minMessage)
    .max(16, maxMessage)
    .required(emptyMessage)
