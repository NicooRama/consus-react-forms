import type {Meta, StoryObj} from '@storybook/react';
import { SignInForm, signInFormDefaultMessages } from './SignInForm'
import { routerDecorator } from '../../../storybook.utils'

const meta: any = {
    title: 'Authentication/SignInForm',
    component: SignInForm,
    tags: ['autodocs'],
    decorators: [routerDecorator]
} satisfies Meta<typeof SignInForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        messages: signInFormDefaultMessages,
        recoverPasswordLink: "/cuenta/recuperar-contrasena",
        activateAccountLink: "/cuenta/reenviar-activacion"
    },
};
