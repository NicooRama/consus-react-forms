import React from 'react'
import { Button, ConsusTheme, FormFrame, LoaderWrapper, Text } from '@consus/react-ui'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { Link } from 'react-router-dom'
import { StandardValidations } from '../../../validations/standard.validations'
import { signInIcon } from '../auth.icons'
import { FormInput } from '../../form-input/FormInput'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${props => props.theme.spacing.sm}px;

  .form-title {
    text-align: center;
  }

  .recover-links {
    display: flex;

    a {
      padding: 0;
      border: 0;
      text-decoration: none;

      &:hover {
        text-decoration: underline;
      }

      &:first-of-type {
        margin-right: auto;
      }

      &:last-of-type {
        text-align: right;
      }
    }
  }

  .submit-container {
    display: flex;
    text-align: right;

    &:last-child {
      margin-left: auto;
    }
  }
`

export interface SignInFormProps {
  onSubmit: (values: object) => Promise<void>;
  messages?: {
    title: string;
    username: {
      label: string;
      error: {
        empty: string;
      }
    },
    password: {
      label: string;
      error: {
        empty: string;
      }
    },
    recoverPassword: string;
    activateAccount: string;
    button: string;
  },
  recoverPasswordLink?: string;
  activateAccountLink?: string;

  [props: string]: any;
}

export const signInFormDefaultMessages = {
  title: 'Ingresar',
  username: {
    label: 'Correo electrónico',
    error: {
      empty: 'Ingrese una dirección de correo electrónico',
    },
  },
  password: {
    label: 'Contraseña',
    error: {
      empty: 'Ingrese una contraseña',
    },
  },
  recoverPassword: 'Olvide mi contraseña',
  activateAccount: 'Reenviar mail de activación',
  button: 'Ingresar',
}

export const SignInForm: React.FC<SignInFormProps> = ({
                                                        onSubmit,
                                                        messages = signInFormDefaultMessages,
                                                        recoverPasswordLink = '/cuenta/recuperar-contrasena',
                                                        activateAccountLink = '/cuenta/reenviar-activacion',
                                                        ...props
                                                      }) => {
  const handleSubmit = async (values: any, { setSubmitting }: any) => {
    try {
      await onSubmit(values)
    } catch (e) {
      setSubmitting(false)
      throw(e)
    }
  }

  const validationSchema = () => {
    return Yup.object().shape({
      username: StandardValidations.mail(messages.username.error.empty),
      password: Yup.string().required(messages.password.error.empty),
    })
  }

  return <Formik
    initialValues={{ username: '', password: '' }}
    validationSchema={validationSchema()}
    enableReinitialize={true}
    onSubmit={handleSubmit}
  >
    {({
        handleSubmit,
        isSubmitting,
      }) => (
      <FormFrame icon={signInIcon} {...props}>
        { /** @ts-ignore **/}
        <StyledForm>
          <Text size={'lg'} weight={'semiBold'} className={'form-title'}>{messages.title}</Text>
          <FormInput type={'email'} maxLength={100} name={'username'} label={messages.username.label} />
          <FormInput type={'password'} maxLength={16} name={'password'} label={messages.password.label} />
          <div className={'recover-links'}>
            <Button tag={Link} to={recoverPasswordLink}
                    variant={'tertiary'}
                    onClick={() => {
                    }}>
              {messages.recoverPassword}
            </Button>
            <Button tag={Link} variant={'tertiary'} to={activateAccountLink} onClick={() => {
            }}>{messages.activateAccount}</Button>
          </div>
          <div className={'submit-container'}>
            <LoaderWrapper loading={isSubmitting}>
              <Button type={'submit'} onClick={handleSubmit}>{messages.button}</Button>
            </LoaderWrapper>
          </div>
        </StyledForm>
      </FormFrame>
    )}
  </Formik>
}
