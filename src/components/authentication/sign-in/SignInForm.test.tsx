import {SignInForm} from "./SignInForm";
import { renderWithRouter } from '../../../../test/test.utils'

describe('<SignInForm />', () => {
    it('renders <SignInForm /> successfully', () => {
        const {getByTestId} = renderWithRouter(<SignInForm data-testid={'sign-in-form'} onSubmit={async() => {}}/>)
        expect(getByTestId('sign-in-form')).toBeInTheDocument();
    });
});
