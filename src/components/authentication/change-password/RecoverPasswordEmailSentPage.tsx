import React from "react"
import {Container, ResultMessage} from '@consus/react-ui';

export interface RecoverPasswordEmailSentPageProps {
  title?: string;
  message: string;
  className?: string;
  [props: string]: any;
}

export const RecoverPasswordEmailSentPage: React.FC<RecoverPasswordEmailSentPageProps> = ({
                                               message,
                                               title = "¡Recuperación en camino!",
                                               className = "",
  ...props
                                             }) => {
  return (
    <Container className={className} {...props}>
      <ResultMessage message={message}
                     title={title}/>
    </Container>
  )
};
