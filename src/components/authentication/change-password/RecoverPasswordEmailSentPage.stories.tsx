import type {Meta, StoryObj} from '@storybook/react';
import {RecoverPasswordEmailSentPage} from './RecoverPasswordEmailSentPage';

const meta: any = {
    title: 'Authentication/Change Password/RecoverPasswordEmailSentPage',
    component: RecoverPasswordEmailSentPage,
    tags: ['autodocs'],
} satisfies Meta<typeof RecoverPasswordEmailSentPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        title: "¡Recuperación en camino!",
        message: 'Hemos enviado un correo electrónico con las instrucciones'
    },
};
