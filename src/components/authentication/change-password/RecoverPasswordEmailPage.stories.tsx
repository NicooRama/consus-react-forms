import type {Meta, StoryObj} from '@storybook/react';
import { RecoverPasswordEmailPage, recoverPasswordEmailPageDefaultMessages } from './RecoverPasswordEmailPage'

const meta: any = {
    title: 'Authentication/Change Password/RecoverPasswordEmailPage',
    component: RecoverPasswordEmailPage,
    tags: ['autodocs'],
} satisfies Meta<typeof RecoverPasswordEmailPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        messages: recoverPasswordEmailPageDefaultMessages
    },
};
