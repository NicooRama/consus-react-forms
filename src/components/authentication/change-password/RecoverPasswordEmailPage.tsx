import React from 'react'
import { Container, fontAwesomeToIcon } from '@consus/react-ui'
import PropTypes from 'prop-types'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { EmailForm, emailFormDefaultMessages, EmailFormMessages } from '../email-form/EmailForm'


export interface RecoverPasswordEmailPageProps {
  onSubmit: ({ username }: { username: string }) => Promise<void>;
  messages?: EmailFormMessages,
  className?: string;

  [props: string]: any;
}

export const recoverPasswordEmailPageDefaultMessages = {
  ...emailFormDefaultMessages,
  title: '¡Recuperemos tu contraseña!',
  button: 'Enviar',
}

export const RecoverPasswordEmailPage = ({
                                           messages = {
                                             ...emailFormDefaultMessages,
                                             title: '¡Recuperemos tu contraseña!',
                                             button: 'Enviar',
                                           },
                                           onSubmit,
                                           className = '',
                                           ...props
                                         }: RecoverPasswordEmailPageProps) => {
  return (<Container className={className} {...props}>
    <EmailForm messages={messages}
               onSubmit={onSubmit}
               icon={fontAwesomeToIcon(faEnvelope)} />

  </Container>)
}
