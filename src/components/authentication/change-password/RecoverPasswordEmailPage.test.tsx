import { RecoverPasswordEmailPage } from './RecoverPasswordEmailPage'
import { renderWithRouter } from '../../../../test/test.utils'

describe('<RecoverPasswordEmailPage />', () => {
    it('renders <RecoverPasswordEmailPage /> successfully', () => {
        const {getByTestId} = renderWithRouter(<RecoverPasswordEmailPage data-testid={'recover-password-email-page'} onSubmit={async () => {}}/>)
        expect(getByTestId('recover-password-email-page')).toBeInTheDocument();
    });
});
