import React from 'react'
import { ResultMessage } from '@consus/react-ui'


export interface ChangePasswordSuccessPageProps {
  onLoginClick: () => void;
  title?: string;
  message: string;
  buttonText?: string;
  className?: string;

  [props: string]: any;
}

//CAMBIAR POR RESULT MESSAGE
export const ChangePasswordSuccessPage: React.FC<ChangePasswordSuccessPageProps> = ({
                                                                                      message,
                                                                                      title = '¡Exito!',
                                                                                      buttonText = 'Ingresar',
                                                                                      onLoginClick,
                                                                                      className = '',
                                                                                      ...props
                                                                                    }) => {
  return (<ResultMessage title={title}
                         buttonText={buttonText}
                         message={message}
                         onButtonClick={onLoginClick}
                         className={className}
                         {...props}
    />
  )
}
