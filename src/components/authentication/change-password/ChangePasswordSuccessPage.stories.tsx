import type {Meta, StoryObj} from '@storybook/react';
import {ChangePasswordSuccessPage} from './ChangePasswordSuccessPage';

const meta: any = {
    title: 'Authentication/Change Password/ChangePasswordSuccessPage',
    component: ChangePasswordSuccessPage,
    tags: ['autodocs'],
} satisfies Meta<typeof ChangePasswordSuccessPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: '¡Contraseña modificada con exito!',
        title: '¡Exito!',
        buttonText: 'Ingresar',
        // eslint-disable-next-line no-console
        onLoginClick: () => console.log('login!')
    },
};
