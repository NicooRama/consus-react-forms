import {ChangePasswordSuccessPage} from "./ChangePasswordSuccessPage";
import { render } from '../../../../test/test.utils'

describe('<ChangePasswordSuccessPage />', () => {
    it('renders <ChangePasswordSuccessPage /> successfully', () => {
        const {getByTestId} = render(<ChangePasswordSuccessPage data-testid={'change-password-success-page'}
                                                                message={'Message'}
                                                                onLoginClick={()=>{}}
        />)
        expect(getByTestId('change-password-success-page')).toBeInTheDocument();
    });
});
