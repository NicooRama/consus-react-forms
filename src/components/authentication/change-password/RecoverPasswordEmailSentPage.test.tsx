import {RecoverPasswordEmailSentPage} from "./RecoverPasswordEmailSentPage";
import { render } from '../../../../test/test.utils'

describe('<RecoverPasswordEmailSentPage />', () => {
    it('renders <RecoverPasswordEmailSentPage /> successfully', () => {
        const {getByTestId} = render(<RecoverPasswordEmailSentPage data-testid={'recover-password-email-sent-page'} message={'Message'}/>)
        expect(getByTestId('recover-password-email-sent-page')).toBeInTheDocument();
    });
});
