import type {Meta, StoryObj} from '@storybook/react';
import { ActivateAccountEmailPage, activationAccountEmailPageMessages } from './ActivateAccountEmailPage'

const meta: any = {
    title: 'Authentication/Activation/ActivateAccountEmailPage',
    component: ActivateAccountEmailPage,
    tags: ['autodocs'],
} satisfies Meta<typeof ActivateAccountEmailPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        messages: activationAccountEmailPageMessages
    },
};
