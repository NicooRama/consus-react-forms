import React from 'react'

import { Container } from '@consus/react-ui'
import { EmailForm, emailFormDefaultMessages, EmailFormMessages } from '../email-form/EmailForm'

export interface ActivationPageProps {
  onSubmit: ({ username }: { username: string }) => Promise<void>;
  messages?: EmailFormMessages,
  className?: string;

  [props: string]: any;
}

export const activationAccountEmailPageMessages = {
  ...emailFormDefaultMessages,
  title: '¡Activemos tu cuenta!',
  button: 'Enviar',
}

export const ActivateAccountEmailPage: React.FC<ActivationPageProps> = ({
                                           messages = activationAccountEmailPageMessages,
                                           onSubmit,
                                           className,
                                           ...props
                                         }) => {
  return (<Container className={className} {...props}>
    <EmailForm messages={messages}
               onSubmit={onSubmit} />
  </Container>)
}
