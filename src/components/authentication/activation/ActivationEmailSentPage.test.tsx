import {ActivationEmailSentPage} from "./ActivationEmailSentPage";
import { render, renderWithFormik } from '../../../../test/test.utils'

describe('<ActivationEmailSentPage />', () => {
    it('renders <ActivationEmailSentPage /> successfully', () => {
        const {getByTestId} = render(<ActivationEmailSentPage data-testid={'activation-email-sent-page'} message={'Message'}/>)
        expect(getByTestId('activation-email-sent-page')).toBeInTheDocument();
    });
});
