import {ActivateAccountEmailPage} from "./ActivateAccountEmailPage";
import { renderWithRouter } from '../../../../test/test.utils'

describe('<ActivateAccountEmailPage />', () => {
    it('renders <ActivateAccountEmailPage /> successfully', () => {
        const {getByTestId} = renderWithRouter(<ActivateAccountEmailPage data-testid={'activate-account-email-page'} onSubmit={async ({username})=>{}}/>)
        expect(getByTestId('activate-account-email-page')).toBeInTheDocument();
    });
});
