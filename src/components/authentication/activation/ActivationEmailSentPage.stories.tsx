import type {Meta, StoryObj} from '@storybook/react';
import {ActivationEmailSentPage} from './ActivationEmailSentPage';

const meta: any = {
    title: 'Authentication/Activation/ActivationEmailSentPage',
    component: ActivationEmailSentPage,
    tags: ['autodocs'],
} satisfies Meta<typeof ActivationEmailSentPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        title: "¡Activacion enviada!",
        message: 'Te enviamos un correo electrónico para que puedas activar tu cuenta'
    },
};
