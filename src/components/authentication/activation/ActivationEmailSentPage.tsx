import React from "react"
import {Container, ResultMessage} from '@consus/react-ui';

export interface ActivationEmailSentPageProps {
  title?: string;
  message: string;
  className?: string;
  [props: string]: any;
}

export const ActivationEmailSentPage: React.FC<ActivationEmailSentPageProps> = ({message, title="¡Activacion enviada!", className = "", ...props}) => {
  return (
    <Container className={className} {...props}>
      <ResultMessage message={message} title={title}/>
    </Container>
  )
};
