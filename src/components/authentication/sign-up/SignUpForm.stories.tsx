import type {Meta, StoryObj} from '@storybook/react';
import { defaultSingUpMessages, SignUpForm } from './SignUpForm'
import { routerDecorator } from '../../../storybook.utils'

const meta: any = {
    title: 'Authentication/SignUpForm',
    component: SignUpForm,
    tags: ['autodocs'],
    decorators: [routerDecorator]
} satisfies Meta<typeof SignUpForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        messages: defaultSingUpMessages
    },
};
