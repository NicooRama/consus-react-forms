import React from 'react'
import { Button, ConsusTheme, FormFrame, LoaderWrapper, Text } from '@consus/react-ui'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { passwordValidation } from '../authentication.validations'
import { StandardValidations } from '../../../validations/standard.validations'
import { FormInput } from '../../form-input/FormInput'
import { signUpIcon } from '../auth.icons'

export const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({ theme }: any) => theme.spacing.sm}px;
  .form-title {
    text-align: center;
  }
`

const TermsAndConditionsText = styled.div`
  font-size: 12px;

  a {
    color: ${({ theme }: any) => theme.colors.text};

    &:hover {
      color: ${({ theme }: any) => theme.colors.primary.normal};
    }
  }
`

export interface SignUpFormProps {
  onSubmit: (values: object) => Promise<void>;
  termsAndConditionsLink?: string;
  messages?: {
    title: string,
    mail: {
      error: {
        empty: string;
      },
      label: string;
    },
    password: {
      error: {
        empty: string,
        min: string,
        max: string,
      },
      label: string,

    },
    terms: {
      description: string;
      button: string;
    }
    button: string
  },

  [props: string]: any;
}

export const defaultSingUpMessages = {
  title: '¡Crea tu cuenta!',
  mail: {
    error: {
      empty: 'Ingrese una dirección de correo electrónico',
    },
    label: 'Correo electrónico',
  },
  password: {
    error: {
      empty: 'Ingrese una contraseña',
      min: 'La contraseña debe tener al menos 6 caracteres',
      max: 'La contraseña debe tener como maximo 16 caracteres',
    },
    label: 'Contraseña',
  },
  terms: {
    description: 'Al registrarte estas de acuerdo con nuestros ',
    button: 'terminos y condiciones',
  },
  button: 'Registrarse',
}

export const SignUpForm: React.FC<SignUpFormProps> = ({
                                                        onSubmit,
                                                        messages = defaultSingUpMessages,
                                                        termsAndConditionsLink = '/terminos-y-condiciones',
                                                        ...props
                                                      }) => {

  const handleSubmit = async (values: any, { setSubmitting }: any) => {
    try {
      await onSubmit(values)
    } catch (e) {
      setSubmitting(false)
      throw(e)
    }
  }

  const validationSchema = () => {
    return Yup.object().shape({
      username: StandardValidations.mail(messages.mail.error.empty),
      password: passwordValidation(messages.password.error.min, messages.password.error.max, messages.password.error.empty),
    })
  }

  return <Formik
    initialValues={{ username: '', password: '' }}
    validationSchema={validationSchema()}
    enableReinitialize={true}
    onSubmit={handleSubmit}
  >
    {({
        handleSubmit,
        isSubmitting,
      }) => (
      <FormFrame icon={signUpIcon} {...props}>
        { /** @ts-ignore **/}
        <StyledForm>
          <Text size={'lg'} weight={'semiBold'} className={'form-title'}>{messages.title}</Text>
          <FormInput type={'email'} maxLength={100} name={'username'} label={messages.mail.label} />
          <FormInput type={'password'} maxLength={16} name={'password'} label={messages.password.label} />
          <TermsAndConditionsText>
            {messages.terms.description} <Link to={termsAndConditionsLink}>{messages.terms.button}</Link>
          </TermsAndConditionsText>
          <LoaderWrapper loading={isSubmitting} center={true}>
            <Button type={'submit'} onClick={handleSubmit}>{messages.button}</Button>
          </LoaderWrapper>
        </StyledForm>
      </FormFrame>
    )}
  </Formik>
}
