import {SignUpForm} from "./SignUpForm";
import { renderWithRouter } from '../../../../test/test.utils'

describe('<SignUpForm />', () => {
    it('renders <SignUpForm /> successfully', () => {
        const {getByTestId} = renderWithRouter(<SignUpForm data-testid={'sign-up-form'} onSubmit={async () => {}}/>)
        expect(getByTestId('sign-up-form')).toBeInTheDocument();
    });
});
