import {PasswordForm} from "./PasswordForm";
import { renderWithRouter } from '../../../../test/test.utils'

describe('<PasswordForm />', () => {
    it('renders <PasswordForm /> successfully', () => {
        const {getByTestId} = renderWithRouter(<PasswordForm data-testid={'password-form'} onSubmit={async ()=>{}}/>)
        expect(getByTestId('password-form')).toBeInTheDocument();
    });
});
