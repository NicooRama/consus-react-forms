import React from "react"
import {Form, Formik} from "formik";
import {Button, ConsusTheme, LoaderWrapper, Text, FormFrame, fontAwesomeToIcon} from "@consus/react-ui"
import * as Yup from 'yup';
import {faLock} from "@fortawesome/free-solid-svg-icons";
import { FormInput } from '../../form-input/FormInput';
import { passwordValidation } from '../authentication.validations';
import styled from 'styled-components';

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${props => props.theme.spacing.sm}px;
  .form-title {
    text-align: center;
  }
`

export interface PasswordFormProps {
  name?: string;
  onSubmit: (values: any) => Promise<void>;
  messages?: {
    title: string,
    password: {
      error: {
        empty: string,
        min: string,
        max: string,
      },
      label: string,

    },
    button: string
  },
  [props: string]: any;
}

export const passwordFormDefaultMessages = {
  title: 'Ingresa tu contraseña',
  password: {
    error: {
      empty: 'Ingrese una contraseña',
      min: 'La contraseña debe tener al menos 6 caracteres',
      max: 'La contraseña debe tener como maximo 16 caracteres',
    },
    label: 'Contraseña',
  },
  button: 'Guardar'
}

export const PasswordForm: React.FC<PasswordFormProps> = ({name = 'password', onSubmit, messages = passwordFormDefaultMessages, ...props}) => {
  const handleSubmit = async (values: any, {setSubmitting}: any) => {
    try {
      await onSubmit(values);
    } catch (e) {
      setSubmitting(false);
      throw(e);
    }
  }

  return <Formik
    initialValues={{[name]: ''}}
    validationSchema={Yup.object().shape({
      [name]: passwordValidation(messages.password.error.min, messages.password.error.max, messages.password.error.empty)
    })}
    enableReinitialize={true}
    onSubmit={handleSubmit}
  >
    {({
        handleSubmit,
        isSubmitting,
      }) => (
      <FormFrame icon={fontAwesomeToIcon(faLock)} {...props}>
        { /** @ts-ignore **/}
        <StyledForm>
          <Text size={'lg'} weight={'semiBold'} className={'form-title'}>{messages.title}</Text>
          <FormInput type={'password'} maxLength={16} name={name} label={messages.password.label}/>
          <LoaderWrapper loading={isSubmitting} center={true}>
            <Button type={'submit'} onClick={handleSubmit}>{messages.button}</Button>
          </LoaderWrapper>
        </StyledForm>
      </FormFrame>
    )}
  </Formik>
};
