import type {Meta, StoryObj} from '@storybook/react';
import { PasswordForm, passwordFormDefaultMessages } from './PasswordForm'
import { FormikDecorator } from '../../../storybook.utils'

const meta: any = {
    title: 'Authentication/PasswordForm',
    component: PasswordForm,
    tags: ['autodocs'],
    decorators: []
} satisfies Meta<typeof PasswordForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test',
        messages: passwordFormDefaultMessages,
        onSubmit: () => {}
    },
};
