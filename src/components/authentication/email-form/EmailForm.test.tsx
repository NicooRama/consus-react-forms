import {EmailForm} from "./EmailForm";
import { renderWithRouter } from '../../../../test/test.utils'

describe('<EmailForm />', () => {
    it('renders <EmailForm /> successfully', () => {
        const {getByTestId} = renderWithRouter(<EmailForm data-testid={'email-form'} onSubmit={async () => {}}/>)
        expect(getByTestId('email-form')).toBeInTheDocument();
    });
});
