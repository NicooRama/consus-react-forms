import React from 'react'
import { Button, ConsusTheme, LoaderWrapper, Text, FormFrame, fontAwesomeToIcon } from '@consus/react-ui'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { StandardValidations } from '../../../validations/standard.validations'
import { FormInput } from '../../form-input/FormInput'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${props => props.theme.spacing.sm}px;

  .form-title {
    text-align: center;
  }
`

export interface EmailFormMessages {
  title: string;
  mail: {
    error: {
      empty: string;
    },
    label: string;
  },
  button: string
}

export interface EmailFormProps {
  onSubmit: (values: any) => Promise<void>;
  icon?: { path: string, viewBox: string };
  messages?: EmailFormMessages;

  [props: string]: any;
}

export const emailFormDefaultMessages = {
  title: '',
  mail: {
    error: {
      empty: 'Ingrese una dirección de correo electrónico',
    },
    label: 'Correo electrónico',
  },
  button: '',
}

export const EmailForm: React.FC<EmailFormProps> = ({
                            icon = fontAwesomeToIcon(faEnvelope),
                            onSubmit,
                            messages = emailFormDefaultMessages,
                            ...props
                          }) => {
  const handleSubmit = async (values: any, { setSubmitting }: any) => {
    try {
      await onSubmit(values)
    } catch (e) {
      setSubmitting(false)
      throw(e)
    }
  }

  const validationSchema = () => {
    return Yup.object().shape({
      username: StandardValidations.mail(messages.mail.error.empty),
    })
  }

  return <Formik
    initialValues={{ username: '' }}
    validationSchema={validationSchema()}
    enableReinitialize={true}
    onSubmit={handleSubmit}
  >
    {({
        handleSubmit,
        isSubmitting,
      }) => (
      <FormFrame icon={icon} {...props}>
        { /** @ts-ignore **/}
        <StyledForm>
          <Text size={'lg'} weight={'semiBold'} className={'form-title'}>{messages.title}</Text>
          <FormInput type={'email'} maxLength={100} name={'username'} label={messages.mail.label} />
          <LoaderWrapper loading={isSubmitting} center={true}>
            <Button type={'submit'} onClick={handleSubmit}>{messages.button}</Button>
          </LoaderWrapper>
        </StyledForm>
      </FormFrame>
    )}
  </Formik>
}
