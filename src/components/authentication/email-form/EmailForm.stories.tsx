import type {Meta, StoryObj} from '@storybook/react';
import {EmailForm} from './EmailForm';
import { FormikDecorator } from '../../../storybook.utils'

const meta: any = {
    title: 'Authentication/EmailForm',
    component: EmailForm,
    tags: ['autodocs'],
} satisfies Meta<typeof EmailForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test',
        messages: {
            title: 'Ingresa tu correo electrónico',
            mail: {
                error: {
                    empty: 'Ingrese una dirección de correo electrónico',
                },
                label: 'Correo electrónico'
            },
            button: 'Aceptar'
        },
        onSubmit: () => {}
    },
};
