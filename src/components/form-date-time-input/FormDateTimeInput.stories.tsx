import type {Meta, StoryObj} from '@storybook/react';
import {FormDateTimeInput} from './FormDateTimeInput';
import { FormikDecorator } from '../../storybook.utils'
import * as Yup from 'yup';

const meta: any = {
    title: 'Inputs/FormDateTimeInput',
    component: FormDateTimeInput,
    tags: ['autodocs'],
    decorators: [
        FormikDecorator(
          {
              schema: {
                  date: Yup.string().required("Ingresa una fecha"),
                  time: Yup.string().required("Selecciona un horario"),
              },
              extraValues: {
                  date: new Date(),
                  time: '',
              }
          }
        )
    ]
} satisfies Meta<typeof FormDateTimeInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'date',
        timeName: 'time'
    },
};
