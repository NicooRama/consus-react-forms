import {FormDateTimeInput} from "./FormDateTimeInput";
import { render, renderWithFormik } from '../../../test/test.utils'
import * as Yup from 'yup';

describe('<FormDateTimeInput />', () => {
    it('renders <FormDateTimeInput /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormDateTimeInput data-testid={'form-date-time-input'}
                                                        name={'date'}
                                                        timeName={'time'}

        />, {
            formikOptions: {
                initialValues: {
                    date: new Date(),
                    time: '',
                },
                validationSchema: Yup.object().shape({
                    date: Yup.string().required("Ingresa una fecha"),
                    time: Yup.string().required("Selecciona un horario"),
                }),
            },
        })
        expect(getByTestId('form-date-time-input')).toBeInTheDocument();
    });
});
