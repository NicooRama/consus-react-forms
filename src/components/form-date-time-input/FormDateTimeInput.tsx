import React, { useEffect, useRef, useState } from 'react'
import { swiperScrollBar, OptionSelector, Button, ConsusTheme, DatePicker, mobileMediaQuery } from '@consus/react-ui'
import moment from 'moment'
import { getIn, useFormikContext } from 'formik'
import styled from 'styled-components'
import { FormControlWrapper } from '../form-control-wrapper/FormControlWrapper'
import { generateQuarters } from './formDateTimeInput.utils'
import { showFormErrors } from '../../utils/form.utils'
import { FormErrorMessage } from '../form-error-message/FormErrorMessage'


export interface FormDateTimeInputProps {
  name?: string;
  timeName?: string;
  label?: string;
  timeOptions?: string[];
  theme?: ConsusTheme;

  [props: string]: any;
}

const Container = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.xs}px;
  flex-direction: row;

  .react-datepicker-wrapper {
    min-width: 113px;
  }

  ${mobileMediaQuery} {
    flex-direction: column;
  }
`

const DatePickerContainer = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.xs}px;
  flex-direction: row;

  button {
    height: fit-content;
    width: fit-content;
  }
`

const StyledOptionSelector = styled(OptionSelector)`
  ${({ theme }: any) => swiperScrollBar(theme)};
`

const defaultTimeOptions: string[] = generateQuarters()

const ErrorMessage = ({ name, timeName }: { name: string, timeName?: string }) => {
  const { touched, errors, submitCount } = useFormikContext()
  if (showFormErrors(touched, errors, submitCount, name)) {
    return <FormErrorMessage>{getIn(errors, name)}</FormErrorMessage>
  }
  if (timeName && showFormErrors(touched, errors, submitCount, timeName)) {
    return <FormErrorMessage>{getIn(errors, timeName)}</FormErrorMessage>
  }
  return <></>
}

export const FormDateTimeInput: React.FC<FormDateTimeInputProps> = ({
                                                                      name = 'date',
                                                                      timeName,
                                                                      timeOptions = defaultTimeOptions,
                                                                      label,
                                                                      ...props
                                                                    }) => {
  const { values, setFieldValue } = useFormikContext()
  const ref = useRef(null)

  useEffect(() => {
    const date = getIn(values, name)
    if (!date) {
      return
    }
    const localeTime = date.toLocaleTimeString()
    const [hour, minutes] = localeTime.split(':')
    setTime([hour, minutes].join(':').padStart(5, '0'), setFieldValue)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getIn(values, name)])

  const getTime = (values: any) => {
    if (timeName) {
      return getIn(values, timeName)
    } else {
      return innerTime
    }
  }

  useEffect(() => {
    const time = getTime(values)
    if (!time) return
    const timeOptionsSelector = (ref?.current as any)?.getElementsByClassName('time-options')[0]
    const buttonsElements = timeOptionsSelector?.getElementsByTagName('button')
    if (buttonsElements) {
      const [hourExpected, minutesExpected] = time.split(':')
      const goalMinutes = Number(minutesExpected)
      const candidateButtons = [...buttonsElements].filter((button: any) => button.innerText && button.innerText.split(':')[0] === hourExpected)
      const closest = candidateButtons.reduce(function(prevButton: any, currentButton: any) {
        const prevMinutes = Number(prevButton.innerText.split(':').pop())
        const currentMinutes = Number(currentButton.innerText.split(':').pop())
        return (Math.abs(currentMinutes - goalMinutes) < Math.abs(prevMinutes - goalMinutes) ? currentButton : prevButton)
      }, null)
      closest?.scrollIntoView()
    }
  }, [getTime(values)])

  const [innerTime, setInnerTime] = useState<string | null>(null)
  const handleChangeDate = (value: Date, values: any, setFieldValue: any) => {
    if (!value) value = new Date()
    const mDate = moment(value)
    const time = getTime(values)
    if (time) {
      const [hours, minutes] = time.split(':')
      mDate.set({ h: hours, m: minutes })
    }
    setFieldValue(name, mDate.toDate())
  }

  const handleChangeTime = (time: string, values: any, setFieldValue: any) => {
    setTime(time, setFieldValue)
    const date = getIn(values, name)
    if (!date || !time) return
    const mDate = moment(date)
    const [hours, minutes] = time.split(':')
    // @ts-ignore
    mDate.set({ h: hours, m: minutes })
    setFieldValue(name, mDate.toDate())
  }

  const setTime = (time: string, setFieldValue: any) => {
    if (timeName) {
      setFieldValue(timeName, time)
    } else {
      setInnerTime(time)
    }
  }

  return (<FormControlWrapper name={name} label={label} errorMessage={<ErrorMessage name={name} timeName={timeName} />} {...props}>
    {({ form, field }: any) => (
      <Container ref={ref}>
        <DatePickerContainer>
          <DatePicker selected={field.value}
                      dateFormat={'dd/MM/yyyy'}
                      showTimeSelect={false}
                      placeholderText={label}
                      inline={false}
                      onChange={(value: Date) => handleChangeDate(value, form.values, form.setFieldValue)}
                      onBlur={() => form.setFieldTouched(field.name, true)}
          />
          {timeName && <Button variant={'secondary'} color={'gray.darken'} onClick={() => {
          }} type={'button'}>{getTime(values)}</Button>}
        </DatePickerContainer>
        {/** @ts-ignore */}
        <StyledOptionSelector className={'time-options'}
                              options={timeOptions}
                              selected={getTime(form.values)}
                              onChange={(value: string) => handleChangeTime(value, form.values, form.setFieldValue)} />
      </Container>
    )}
  </FormControlWrapper>)
}
