export const generateQuarters = () => {
    const times = [];
    for (let i = 0; i < 24; i++) {
        for (let j = 0; j < 60; j = j + 15) {
            times.push(`${i.toString().padStart(2, '0')}:${j.toString().padStart(2, '0')}`)
        }
    }
    return times;
}
