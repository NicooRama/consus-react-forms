import { FormSelect } from './FormSelect'
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormSelect />', () => {
  it('renders <FormSelect /> successfully', () => {
    const { getByTestId } = renderWithFormik(<FormSelect data-testid={'form-select'} name={'test'}
                                               options={['Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5']}
    />)
    expect(getByTestId('form-select')).toBeInTheDocument()
  })
})
