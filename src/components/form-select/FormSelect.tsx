import React from "react"
import {Select} from "@consus/react-ui";
import {FormControlProps} from "../formControl.props";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";


export interface FormSelectProps extends FormControlProps {
  options: any[],
}

export const FormSelect: React.FC<FormSelectProps> = ({name, label, fast, options, className = "", ...props}) => {
  return (<FormControlWrapper name={name} label={label} className={className} fast={fast}>
    {({form, field}: any) => (
      <Select className={`form-input-control`}
              items={options}
              onChange={(selectedValue) => {
                form.setFieldValue(name, selectedValue);
              }}
              selected={field.value}
              {...props}
      />
    )}
  </FormControlWrapper>)
};
