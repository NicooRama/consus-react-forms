import type {Meta, StoryObj} from '@storybook/react';
import {FormSelect} from './FormSelect';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Form options/FormSelect',
    component: FormSelect,
    tags: ['autodocs'],
    decorators: [
        FormikDecorator()
    ]
} satisfies Meta<typeof FormSelect>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test',
        options: ["Test 1", "Test 2", "Test 3", "Test 4", "Test 5"]
    },
};
