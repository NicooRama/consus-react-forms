import type {Meta, StoryObj} from '@storybook/react';
import {FormCheckbox} from './FormCheckbox';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Form options/FormCheckbox',
    component: FormCheckbox,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormCheckbox>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'booleanTest',
        label: 'Value'
    },
};
