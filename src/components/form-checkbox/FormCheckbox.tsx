import React from "react"
import { Checkbox } from "@consus/react-ui";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";
import {FormControlProps} from "../formControl.props";


export const FormCheckbox: React.FC<FormControlProps> = ({
                               name,
                               className = "",
                               label,
                               fast,
                               ...props
                             }) => {
  return (<FormControlWrapper name={name} className={className} fast={fast}>
    {({form, field}: any) => (
      <Checkbox checked={field.value}
                onChange={(checked: boolean) => form.setFieldValue(name, checked)}
                //@ts-ignore
                onBlur={form.handleBlur(name)}
                name={name}
                {...props}
      >{label}</Checkbox>
    )}
  </FormControlWrapper>)
};
