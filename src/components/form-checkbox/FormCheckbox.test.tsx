import { FormCheckbox } from './FormCheckbox'
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormCheckbox />', () => {
    it('renders <FormCheckbox /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormCheckbox data-testid={'form-checkbox'}
                                                    name={'booleanTest'}
                                                    label={'Value'}
        />)
        expect(getByTestId('form-checkbox')).toBeInTheDocument();
    });
});
