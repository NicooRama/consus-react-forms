import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  label.form-label {
      margin-bottom: ${({theme}: any) => theme.spacing.xxs}px;
      display: inline-block;
  }
`
export const ConsusFormGlobalStyles = () => (
    <GlobalStyles />
)
