import type {Meta, StoryObj} from '@storybook/react';
import {FormTypeahead} from './FormTypeahead';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormTypeahead',
    component: FormTypeahead,
    tags: ['autodocs'],
    decorators: [FormikDecorator()]
} satisfies Meta<typeof FormTypeahead>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test',
        options: ["Item 1","Item 2","Item 3","Item 4","Item 5"]
    },
};

export const Multi: Story = {
    args: {
        name: 'multiTest',
        multiple: true,
        options: ["Item 1","Item 2","Item 3","Item 4","Item 5"]
    },
};

export const Async: Story = {
    args: {
        name: 'test',
        options: ["Item 1","Item 2","Item 3","Item 4","Item 5"]
    },
};
