import {FormTypeahead} from "./FormTypeahead";
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormTypeahead />', () => {
    it('renders <FormTypeahead /> successfully', () => {
        //@ts-ignore
        const {getByTestId} = renderWithFormik(<FormTypeahead data-testid={'form-typeahead'}
                                                              name={'test'}
                                                              options={['Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5']}
        />)
        expect(getByTestId('form-typeahead')).toBeInTheDocument();
    });
});
