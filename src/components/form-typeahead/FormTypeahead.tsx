import React from "react"
import {ConsusTheme, Input, Typeahead} from "@consus/react-ui";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";
import {FormControlProps} from "../formControl.props";
import {TypeaheadProps as ReactTypeaheadProps} from "react-bootstrap-typeahead/types/types";

export interface FormTypeaheadProps extends FormControlProps, ReactTypeaheadProps {
  handleOnInputChange?: boolean,
  autocomplete?: boolean,
  onSelect?: (item: any | any[]) => void,
  options: any[],
  render: any,
}

export const FormTypeahead: React.FC<FormTypeaheadProps> = ({
                                name,
                                label,
                                className = '',
                                handleOnInputChange = false,
                                onSelect = () => {
                                },
                                multiple,
                                options,
                                autocomplete = false,
                                render,
                                fast,
                                ...props
                              }) => {
  const handleChange = (setFieldValue: (name: string, value: any) => void) => (selected: any) => {
    onSelect(multiple ? selected : selected[0]);
    setFieldValue(name, multiple ? selected : selected[0])
  };

  const handleInputChange = (setFieldValue: (name: string, value: any) => void) =>
    (handleOnInputChange ? (text: any) => setFieldValue(name, text) : undefined);

  const selectByProps = (value: any) => {
    if (!value) return [];
    if (Array.isArray(value) && value.length > 0) return value;
    if (!Array.isArray(value)) return [value];
    return [];
  };

  const inputProps = props?.inputProps || {};
  if (props?.inputProps) {
    (props as any).inputProps = undefined
  }

  const dataTestId = props['data-testid'];
  delete props['data-testid'];

  return (
    <FormControlWrapper name={name} label={label} className={className} fast={fast} data-testid={dataTestId}>
      {({form, field}: any) => (
        <Typeahead className={'form-input-control'}
                   id={name}
                   options={options}
          // @ts-ignore
                   labelKey={render}
          // @ts-ignore
                   onInputChange={handleInputChange(form.setFieldValue)}
          // @ts-ignore
                   onChange={handleChange(form.setFieldValue)}
          // @ts-ignore
                   onBlur={form.handleBlur(name)}
          // @ts-ignore
                   selected={selectByProps(field.value)}
                   multiple={multiple}
          // @ts-ignore
                   inputProps={{autoComplete: autocomplete ? 'on' : 'off', ...inputProps}}
                   {...props}/>
      )}
    </FormControlWrapper>
  )
};
