import type {Meta, StoryObj} from '@storybook/react';
import {FormInputSkeleton} from './FormInputSkeleton';

const meta: any = {
    title: 'Inputs/FormInput/FormInputSkeleton',
    component: FormInputSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof FormInputSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
