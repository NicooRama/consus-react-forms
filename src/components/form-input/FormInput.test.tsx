import { FormInput } from './FormInput'
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormInput />', () => {
    it('renders <FormInput /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormInput data-testid={'form-input'} name={'name'}/>)
        expect(getByTestId('form-input')).toBeInTheDocument();
    });
});
