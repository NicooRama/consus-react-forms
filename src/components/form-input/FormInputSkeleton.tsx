import React from "react"
import {SkeletonInput, SkeletonLabel } from '@consus/react-ui';

export interface FormInputSkeletonProps {
  showLabel?: boolean;
  labelWidth?: string;
  [props: string]: any;
}

export const FormInputSkeleton: React.FC<FormInputSkeletonProps> = ({showLabel = true, labelWidth='100%', ...props}) => {
    return (<div {...props}>
    {
      showLabel && <SkeletonLabel  width={labelWidth}/>
    }
    <SkeletonInput />
  </div>)
};
