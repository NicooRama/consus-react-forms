import {FormInputSkeleton} from "./FormInputSkeleton";
import {render} from "../../../test/test.utils";

describe('<FormInputSkeleton />', () => {
    it('renders <FormInputSkeleton /> successfully', () => {
        const {getByTestId} = render(<FormInputSkeleton data-testid={'form-input-skeleton'}/>)
        expect(getByTestId('form-input-skeleton')).toBeInTheDocument();
    });
});
    