import type {Meta, StoryObj} from '@storybook/react';
import {FormInput} from './FormInput';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormInput/FormInput',
    component: FormInput,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test'
    },
};
