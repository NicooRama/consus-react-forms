import React from "react"
import {Input} from "@consus/react-ui";
import {FormControlProps} from "../formControl.props";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";

//TODO: add custom on change handler
export const FormInput: React.FC<FormControlProps> = ({
                              name,
                              className = "",
                              label,
                              fast,
                              changeHandler = (value: string) => value,
                              ...props
                          }: FormControlProps) => {
    const onTextChange = (formHandleChange: any) => (value: string) => {
        const decoratedValue = changeHandler(value);
        formHandleChange(decoratedValue);
    }

    return (
        <FormControlWrapper name={name} label={label} className={className} fast={fast}>
             {({form, field}: any) => (
                <Input className={'form-input-control'}
                       onTextChange={onTextChange(form.handleChange(name))}
                       onBlur={form.handleBlur(name)}
                       value={field.value}
                       name={name}
                       {...props}/>
            )}
        </FormControlWrapper>
    )
};
