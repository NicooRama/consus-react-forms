import type {Meta, StoryObj} from '@storybook/react';
import {FormControlWrapper} from './FormControlWrapper';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Wrappers/FormControlWrapper',
    component: FormControlWrapper,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormControlWrapper>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: () => <div></div>,
        name: 'test'
    },
};
