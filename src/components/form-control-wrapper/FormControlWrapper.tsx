import React from "react"
import {FastField, Field, getIn} from "formik";
import {FormControlProps} from "../formControl.props";
import {FormErrorMessage} from "../form-error-message/FormErrorMessage";
import { showFormErrors } from '../../utils/form.utils'

export interface FormControlWrapperProps extends FormControlProps {
    children: any,
    errorMessage?: any;
}

export const FormControlWrapper: React.FC<FormControlWrapperProps> = ({
                                       label,
                                       name,
                                       className = "",
                                       children,
                                       errorMessage: ErrorMessage,
                                       fast = false,
                                        ...props
                                   }) => {
    const FormikField = fast ? FastField : Field;

    return (<div className={className} {...props}>
        {label && <label htmlFor={name} className={'form-label'}>{label}</label>}
        <FormikField name={name}>
            {({field, form}: any) => {
                return (
                    <div>
                        {
                            children({form, field})
                        }
                        {
                            !ErrorMessage ?
                                (showFormErrors(form.touched, form.errors, form.submitCount, name) &&
                                    <FormErrorMessage>{getIn(form.errors, name)}</FormErrorMessage>) : ErrorMessage
                        }
                    </div>
                )
            }}
        </FormikField>
    </div>)
};
