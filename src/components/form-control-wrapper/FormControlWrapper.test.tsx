import { FormControlWrapper } from './FormControlWrapper'
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormControlWrapper />', () => {
    it('renders <FormControlWrapper /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormControlWrapper data-testid={'form-control-wrapper'}
                                                          name={'test'}

        >
            {() => <div></div>}
        </FormControlWrapper>)
        expect(getByTestId('form-control-wrapper')).toBeInTheDocument();
    });
});
