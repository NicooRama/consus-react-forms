import {FormTextArea} from "./FormTextArea";
import { render, renderWithFormik } from '../../../test/test.utils'

describe('<FormTextArea />', () => {
    it('renders <FormTextArea /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormTextArea data-testid={'form-text-area'} name={'test'}/>)
        expect(getByTestId('form-text-area')).toBeInTheDocument();
    });
});
