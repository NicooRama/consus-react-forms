import React from "react"
import {Input} from "@consus/react-ui";
import {FormControlProps} from "../formControl.props";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";

//TODO: add custom on change handler
export const FormTextArea: React.FC<FormControlProps> = ({
                               name,
                               className = "",
                               label,
                               fast,
                               changeHandler = (value: string) => value,
                               ...props
                             }) => {
  const onTextChange = (formHandleChange: any) => (value: string) => {
    const decoratedValue = changeHandler(value);
    formHandleChange(decoratedValue);
  }

  const dataTestId = props['data-testid'];
  delete props['data-testid'];

  return (
    <FormControlWrapper name={name} label={label} className={className} fast={fast} data-testid={dataTestId}>
      {({form, field}: any) => (
        <Input className={'form-input-control'}
               onTextChange={onTextChange(form.handleChange(name))}
               onBlur={form.handleBlur(name)}
               value={field.value}
               name={name}
               tag={'textarea'}
               rows={5}
               {...props}/>
      )}
    </FormControlWrapper>
  )
};
