import type {Meta, StoryObj} from '@storybook/react';
import {FormTextArea} from './FormTextArea';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormTextArea',
    component: FormTextArea,
    tags: ['autodocs'],
    decorators: [ FormikDecorator() ],
} satisfies Meta<typeof FormTextArea>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'test'
    },
};
