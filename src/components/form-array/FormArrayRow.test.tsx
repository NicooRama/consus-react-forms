import {FormArrayRow} from "./FormArrayRow";
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormArrayRow />', () => {
    it('renders <FormArrayRow /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormArrayRow
          data-testid={'form-array-row'}
          name={'multiTest.0'}
          value={'test'}
          index={0}
          AddButton={({className}) => <button className={className}>Add</button>}
          RemoveButton={({className}) => <button className={className}>Remove</button>}
        />)
        expect(getByTestId('form-array-row')).toBeInTheDocument();
    });
});
