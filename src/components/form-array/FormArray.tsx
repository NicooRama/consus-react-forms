import React, {ReactElement} from "react"
import PropTypes from 'prop-types';
import {FieldArray, getIn} from 'formik';
import {IconButton, PlusButton} from '@consus/react-ui';
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import styled from "styled-components";

const StyledPlusButton = styled(PlusButton)`
  margin-top: ${({theme}) => theme.spacing.sm}px;
`;


export type FormArrayButton = ({className}: { className: string }) => ReactElement

export interface FormArrayProps {
  name: string,
  children: ({
               value,
               index,
               name,
               AddButton,
               RemoveButton,
             }: {
    value: any,
    index: number,
    name: string,
    AddButton: FormArrayButton,
    RemoveButton: FormArrayButton,
    [props: string]: any,
  }) => ReactElement,
  className?: string,
  onNewElement: () => any,
}

export const FormArray: React.FC<FormArrayProps> = ({
                            name,
                            children,
                            onNewElement,
                            className = "",
                              ...props
                          }) => {
  const removeButton = (onClick: () => void) => ({className = ""}: any) => (
    <IconButton
      className={className}
      circle
      color="error.normal"
      icon={faTrashAlt}
      onClick={onClick}
      variant="secondary"
    />
  )

  const addButton = (onClick: () => void) => ({className = ""}: any) => (
    <IconButton
      className={className}
      circle
      color="success.dark"
      icon={faPlus}
      onClick={onClick}
      variant="secondary"
    />
  )

  return <FieldArray name={name} render={
    ({move, swap, push, insert, unshift, pop, remove, form}) => {
      const values = getIn(form.values, name);
      return <div className={className} {...props}>
        <div>
          {
            values && values.map((value: any, index: number) => (
              children({
                value,
                index,
                name: `${name}.${index}`,
                AddButton: addButton(() => {
                  insert(index + 1, onNewElement());
                }),
                RemoveButton: removeButton(() => {
                  remove(index)
                })
              })
            ))
          }
        </div>
        {/** @ts-ignore **/}
        <StyledPlusButton type={'button'} className={'new-element-button'} onClick={(e: any) => {
          e.stopPropagation();
          push(onNewElement())
        }
        }/>
      </div>
    }}/>
};

FormArray.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  onNewElement: PropTypes.func.isRequired,
  children: PropTypes.func.isRequired,
};
