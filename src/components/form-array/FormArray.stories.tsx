import type {Meta, StoryObj} from '@storybook/react';
import {FormArray} from './FormArray';
import { FormikDecorator } from '../../storybook.utils'
import { FormArrayRow } from './FormArrayRow'

const meta: any = {
    title: 'Form Array/FormArray',
    component: FormArray,
    tags: ['autodocs'],
    decorators: [FormikDecorator()]
} satisfies Meta<typeof FormArray>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'multiTest',
        onNewElement: () => '',
        children: FormArrayRow,
    },
};
