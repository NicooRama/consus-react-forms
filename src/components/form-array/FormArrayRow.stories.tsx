import type { Meta, StoryObj } from '@storybook/react'
import { FormArrayRow } from './FormArrayRow'
import { FormikDecorator } from '../../storybook.utils'
import { IconButton } from '@consus/react-ui'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons'

const meta: any = {
  title: 'Form Array/FormArrayRow',
  component: FormArrayRow,
  tags: ['autodocs'],
  decorators: [FormikDecorator()],
} satisfies Meta<typeof FormArrayRow>

export default meta
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    name: 'multiTest.0',
    index: 0,
    AddButton: ({className}) => <IconButton
      className={className}
      circle
      color='success.dark'
      icon={faPlus}
      // eslint-disable-next-line no-console
      onClick={() => console.log('insert(index + 1, onNewElement())')}
      variant='secondary'
    />,
    RemoveButton: ({className}) => <IconButton
      className={className}
      circle
      color='error.normal'
      icon={faTrashAlt}
      // eslint-disable-next-line no-console
      onClick={() => console.log('remove(index)')}
      variant='secondary'
    />,
  },
}
