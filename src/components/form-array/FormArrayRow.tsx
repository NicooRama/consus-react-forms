import PropTypes from 'prop-types'
import { FormInput } from '../form-input/FormInput'
import React from 'react'
import { FormArrayButton } from './FormArray'
import styled from 'styled-components'
import { useFormikContext, getIn } from 'formik'

const Container = styled.div`
  display: flex;
  align-items: center;

  &.form-array-row + .form-array-row {
    margin-top: ${({ theme }) => theme.spacing.xs}px;
  }

  .form-array-add-button, .form-array-input {
    margin-right: ${({ theme }) => theme.spacing.xs}px;
  }

  .form-array-input {
    width: 100%;
  }
`

export interface FormArrayRowProps {
  value: any;
  name: string,
  index: number,
  AddButton: FormArrayButton,
  RemoveButton: FormArrayButton,
  placeholder?: string,
  className?: string,
  fast?: boolean,
  [props: string]: any;
}

export const FormArrayRow: React.FC<FormArrayRowProps> = ({
                                                            name,
                                                            index,
                                                            AddButton,
                                                            RemoveButton,
                                                            placeholder = 'Valor',
                                                            className = '',
                                                            fast = true,
                                                            value,
                                                            ...props
                                                          }) => {
  const dataTestId = props['data-testid'];
  delete props['data-testid'];
  return (<Container key={index} className={`form-array-row ${className}`} data-testid={dataTestId}>
    <FormInput name={name}
               className={'form-array-input'}
               fast={fast}
      // @ts-ignore
               placeholder={`${placeholder} ${index + 1}`}
               value={value}
               {...props}
    />
    <AddButton className={'form-array-add-button'} />
    <RemoveButton className={'form-array-remove-button'} />
  </Container>)
}

FormArrayRow.propTypes = {
  name: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  AddButton: PropTypes.func.isRequired,
  RemoveButton: PropTypes.func.isRequired,
  fast: PropTypes.bool,
  className: PropTypes.string,
}
