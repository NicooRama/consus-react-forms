import { FormArray } from './FormArray'
import { renderWithFormik } from '../../../test/test.utils'
import { FormArrayRow } from './FormArrayRow'

describe('<FormArray />', () => {
  it('renders <FormArray /> successfully', () => {
    const { getByTestId } = renderWithFormik(<FormArray data-testid={'form-array'}
                                              name={'multiTest'}
                                              onNewElement={() => ''}

    >{FormArrayRow as any}</FormArray>)
    expect(getByTestId('form-array')).toBeInTheDocument()
  })
})
