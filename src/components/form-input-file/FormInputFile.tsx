import React, { useState } from 'react'
import { FormErrorMessage } from '../form-error-message/FormErrorMessage'
import { FormControlWrapperProps } from '../form-control-wrapper/FormControlWrapper'
import { Field, getIn } from 'formik'
import { IconButton } from '@consus/react-ui'
import { faUpload } from '@fortawesome/free-solid-svg-icons'
import styled, { css } from 'styled-components'
import { showFormErrors } from '../../utils/form.utils'


const StyledIconButton = styled(IconButton)`
  ${({ hasLabel }) =>
          !hasLabel &&
          css`
            gap: 0
          `}
`

export interface FormInputFileProps extends Omit<FormControlWrapperProps, 'children'> {
  maxSize?: number;//en mb
  maxQuantity?: number;
  validator?: (files: File[]) => string | boolean;
  buttonProps?: object;
  messages?: {
    size: string;
    quantity: string;
  }
}

/**
 * Por ahora solo soporta un archivo y lo pasa a base64
 * @param label
 * @param name
 * @param className
 * @param maxSize
 * @param maxQuantity
 * @param ErrorMessage
 * @param messages
 * @param validator
 * @param buttonProps
 * @param props
 * @constructor
 */
export const FormInputFile: React.FC<FormInputFileProps> = ({
                                                              label,
                                                              name,
                                                              className = '',
                                                              maxSize = 2,
                                                              maxQuantity = 1,
                                                              errorMessage: ErrorMessage,
                                                              messages = {
                                                                size: `El archivo especificado debe tener como maximo {maxSize} mb`,
                                                                quantity: `Solo puedes subir {maxQuantity} archivos`,
                                                              },
                                                              validator,
                                                              buttonProps = {},
                                                              ...props
                                                            }) => {
  const [errorMessage, setErrorMessage] = useState('')

  const handleChange = (e: any, setFieldValue: any) => {
    setErrorMessage('')
    setFieldValue(name, '')
    const files = e.target.files
    if (validator) {
      const message = validator(files)
      if (message !== true) {
        if (message !== '') {
          setErrorMessage(message || 'El/los archivos subidos contienen errores')
          return
        }
      }
    }
    const file = files[0]
    const fileSize = file.size / 1024 / 1024
    if (files.length > maxQuantity) {
      setErrorMessage(messages.size.replace('{maxSize}', maxSize.toString()))
      return
    }
    if (fileSize >= maxSize) {
      setErrorMessage(messages.size.replace('{maxSize}', maxSize.toString()))
      return
    }
    const fileReader = new FileReader()
    fileReader.onload = () => {
      if (fileReader.readyState === 2) {
        setFieldValue(name, fileReader.result)
      }
    }
    fileReader.readAsDataURL(file)
  }

  return (<div className={className}>
    <Field name={name}>
      {({ form }: any) => {
        return (
          <div>
            <label htmlFor={name}>
              <StyledIconButton tag={'span'} onClick={() => {
              }} icon={faUpload} hasLabel={!!label} variant={'secondary'} {...buttonProps}>
                <>
                  {!!label && label}
                  <input type={'file'}
                         name={name}
                         id={name}
                         accept='image/*'
                         hidden
                         multiple={maxQuantity > 1}
                         onChange={(e) => handleChange(e, form.setFieldValue)}
                         {...props}
                  />
                </>
              </StyledIconButton>
            </label>
            {
              !ErrorMessage ?
                ((showFormErrors(form.touched, form.errors, form.submitCount, name) || errorMessage) &&
                  <FormErrorMessage>{errorMessage || getIn(form.errors, name)}</FormErrorMessage>) : ErrorMessage
            }
          </div>
        )
      }}
    </Field>
  </div>)
}
