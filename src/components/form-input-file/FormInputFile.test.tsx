import {FormInputFile} from "./FormInputFile";
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormInputFile />', () => {
    it('renders <FormInputFile /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormInputFile data-testid={'form-input-file'} name={'test'} label={'Subir archivo'}/>)
        expect(getByTestId('form-input-file')).toBeInTheDocument();
    });
});
