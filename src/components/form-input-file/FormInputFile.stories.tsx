import type {Meta, StoryObj} from '@storybook/react';
import {FormInputFile} from './FormInputFile';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Inputs/FormInputFile',
    component: FormInputFile,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormInputFile>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        label: 'Subir archivo',
        name: 'test'
    },
};
