import type {Meta, StoryObj} from '@storybook/react';
import {FormCheckOptionSelector} from './FormCheckOptionSelector';
import { FormikDecorator } from '../../storybook.utils'

const meta: any = {
    title: 'Form options/FormCheckOptionSelector',
    component: FormCheckOptionSelector,
    tags: ['autodocs'],
    decorators: [FormikDecorator()],
} satisfies Meta<typeof FormCheckOptionSelector>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        name: 'multiTest',
        options: ["Test 1", "Test 2", "Test 3", "Test 4", "Test 5"]
    },
};
