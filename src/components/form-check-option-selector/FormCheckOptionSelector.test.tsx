import { FormCheckOptionSelector } from './FormCheckOptionSelector'
import { renderWithFormik } from '../../../test/test.utils'

describe('<FormCheckOptionSelector />', () => {
    it('renders <FormCheckOptionSelector /> successfully', () => {
        const {getByTestId} = renderWithFormik(<FormCheckOptionSelector data-testid={'form-check-option-selector'}
                                                              name={'multiTest'}
                                                              options={["Test 1", "Test 2", "Test 3", "Test 4", "Test 5"]}
        />)
        expect(getByTestId('form-check-option-selector')).toBeInTheDocument();
    });
});
