import React from "react"
import PropTypes from 'prop-types';
import {CheckOptionsSelector} from "@consus/react-ui";
import {FormControlWrapper} from "../form-control-wrapper/FormControlWrapper";
import {FormControlProps} from "../formControl.props";

export interface FormCheckOptionSelectorProps extends FormControlProps {
  options: any[],
  children?: any,
  onChange?: () => void,

  }

export const FormCheckOptionSelector: React.FC<FormCheckOptionSelectorProps> = ({
                                          name,
                                          label,
                                          options,
                                          onChange,
                                          className = "",
                                          fast,
                                          children,
                                          ...props
                                        }) => {
  const handleChange = (setFieldValue: any) => ((selecteds: any) => {
    setFieldValue(name, selecteds)
  });

  return (<FormControlWrapper name={name} label={label} fast={fast}>
      {({form, field}: any) => (
        <CheckOptionsSelector options={options}
                              onChange={onChange || handleChange(form.setFieldValue)}
                              selecteds={field.value}
                              {...props}
        >
          {children}
        </CheckOptionsSelector>
      )}
    </FormControlWrapper>
  )
};

FormCheckOptionSelector.propTypes = {
  className: PropTypes.string,
};
