import { BrowserRouter } from 'react-router-dom'
import {Form, Formik} from "formik";
import * as Yup from 'yup';
import {Button} from "@consus/react-ui";
export const routerDecorator = (Story: any) => (
  <BrowserRouter>
    <Story />
  </BrowserRouter>
)

function range(start: number, end: number) {
  return Array(end - start + 1)
    .fill('')
    .map((_, idx) => start + idx)
}

export const makeControlForObjectArray = (
  items: any[],
  key: string,
  type = 'select',
  defaultValue: any = null,
) => {
  return {
    options: range(0, items.length - 1),
    mapping: items,
    control: {
      type,
      labels: items.reduce((acumm, value, index) => {
        acumm[index] = value[key]
        return acumm
      }, {}),
    },
    defaultValue,
  }
}

export const makeControlForRender = (item: any, key: string) => ({
  options: Object.keys(item),
  defaultValue: key,
  control: { type: 'select' },
})

export const disableDummyArgTypes = {
  as: {
    table: {
      table: {
        disable: true,
      },
    },
  },
  forwardedAs: {
    table: {
      table: {
        disable: true,
      },
    },
  },
  ref: {
    table: {
      table: {
        disable: true,
      },
    },
  },
  theme: {
    table: {
      table: {
        disable: true,
      },
    },
  },
}

const handleSubmit = (values: any) => {
    console.log(values);
}
export const FormikDecorator = ({
                                  schema = {
                                    test: Yup.string().required("Ingrese un nombre"),
                                    multiTest: Yup.array(),
                                  } as any,
                                  extraValues = {} as any,
                                  showButton = true,
                                } = {}) => (Story: any) => (
    <Formik
        initialValues={{
          test: '',
          multiTest: [],
          booleanTest: false,
          ...extraValues,
        }}
        validationSchema={Yup.object().shape(schema)}
        enableReinitialize={true}
        onSubmit={handleSubmit}
    >
      {({
          handleSubmit,
          isSubmitting,
          values,
          errors
        }) => (
          <Form>
            <div style={{display: 'flex', gap: 16, flexDirection: 'column', flexWrap: 'wrap'}}>
              <Story/>
              {/** @ts-ignore **/}
              {showButton && <Button type={'submit'} onClick={handleSubmit}>Aceptar</Button>}
              <FormikDebugPanel values={values} errors={errors}/>
            </div>
          </Form>
      )}
    </Formik>
);

export const FormikDebugPanel = ({values, errors}: any) => {
    return <div>
        <div>
            {JSON.stringify(values)}
        </div>
        <div>
            {JSON.stringify(errors)}
        </div>
    </div>
}
