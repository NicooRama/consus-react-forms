export const youtubeNormalizeUrl = (url: string) => url.replace("watch?v=","embed/");
export const youtubeRegexValidation = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
