import * as Yup from "yup";
import {alphanumericSpace} from "./regularExpressions";
import validator from 'validator';
import {StrUtils} from "@consus/js-utils";
import { StringSchema } from 'yup'

const mail = (msg = "Debe ingresar un mail", formatErrorMsg = "El mail ingresado es incorrecto"): StringSchema => {
    return Yup.string()
      .test('mail', formatErrorMsg, (value: any) => {
          if(StrUtils.isEmpty(value)) return true;
          return validator.isEmail(value);
      }).required(msg);
};

function equalTo(ref: any, msg: string) {
    return Yup.mixed().test({
        name: 'equalTo',
        exclusive: false,
        // eslint-disable-next-line
        message: msg || "${path} must be the same as ${reference}",
        params: {
            reference: ref.path,
        },
        test: function(value) {
            return value === this.resolve(ref);
        },
    });
}

// @ts-ignore
Yup.addMethod(Yup.string, 'equalTo', equalTo);

export const StandardValidations = {
    nameSpace: (required = true, mensaje = "Debe ingresar un nombre") => {
        let validation = Yup.string().matches(alphanumericSpace, "El nombre solo puede contener letras, numeros y espacios");
        if(required){
            validation = validation.required(mensaje);
        }
        return validation;
    },
    number: (max = 1000, required = true, requiredMsg = "Ingrese un número") => {
        let yup = Yup.number();
        if (required) { yup = yup.typeError(requiredMsg) }
        yup = yup.max(max, `Debe ser menor a ${max}`).positive("Ingrese un número mayor a 0");

        return required ? yup.required("Ingrese un número") : yup;
        },
    objectRequired: (msg = "Seleccione una opcion") => Yup.object().nullable().required(msg),
    date: (msg = "Ingrese una fecha y hora") => Yup.date().typeError(msg).required(msg),
    string: (msg: string) => Yup.string().required(msg),
    mail,
    password: (msg = "Debe ingresar una contraseña") => Yup.string().nullable().min(8, "La contraseña debe tener al menos 8 caracteres").required(msg),
    confirmPassword: (ref = 'password') => (Yup.string() as any).equalTo(Yup.ref(ref), 'Las contraseñas ingresadas no coinciden').required('Debe reingresar la contraseña'),
};
