export default {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.ts?$": "ts-jest",
  },
  moduleNameMapper: {
    '\\.(gif|ttf|eot|svg|png|jpg|css|less|sass|scss)$': '<rootDir>/test/__mocks__/fileMock.js',
  },
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
}

/**
 * const uuid = require('../node_modules/uuid/dist');
 * module.exports = uuid;
 */
