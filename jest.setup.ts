import '@testing-library/jest-dom/extend-expect';
import styled from 'styled-components';

import { TextDecoder, TextEncoder } from 'util';

global.TextEncoder = TextEncoder as any;
global.TextDecoder = TextDecoder as any;
