import type { Decorator, Preview } from '@storybook/react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../src/utils/theme'
import { ConsusGlobalStyles } from '@consus/react-ui'
import { ConsusFormGlobalStyles } from '../src/components/ConsusFormGlobalStyles'

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
}

const withTheme: Decorator = (StoryFn, context) => {
  return (
    <ThemeProvider theme={theme}>
      <StoryFn />
    </ThemeProvider>
  )
}

const withGlobalStyles: Decorator = (StoryFn, context) => {
  return (
    <div>
      <ThemeProvider theme={theme}>
        <ConsusGlobalStyles />
        <ConsusFormGlobalStyles />
      </ThemeProvider>
      <StoryFn />
    </div>
  )
}

export const decorators = [
  withTheme,
  withGlobalStyles,
]

export default preview
